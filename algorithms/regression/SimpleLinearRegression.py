import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.cross_validation import train_test_split as TrainTestSplit
from sklearn.linear_model import LinearRegression

import algorithms.visualize.Plot as visualize

# import data set
dataset = pd.read_csv('resources/02_4_Salary_Data.csv')
iVariables = dataset.iloc[:, :-1].values
dVariables = dataset.iloc[:, 1].values

# split into training and test sets (80:20)
iTrain, iTest, dTrain, dTest = TrainTestSplit(iVariables, dVariables, test_size=1 / 3, random_state=0)

# remove feature bias (i.e. feature scaling)
# n/a: library does it for us

# build model
model = LinearRegression()
model.fit(iTrain, dTrain)

# check predictions (manually)
dPrediction = model.predict(iTest)

# visualize model (2-dimensions)
visualize.plotLinearRegression(model, iTrain, iTrain, dTrain, "Experience", "Salary", "Salary vs Experience (Train Data)")
visualize.plotLinearRegression(model, iTrain, iTest, dTest, "Experience", "Salary", "Salary vs Experience (Test Data)")

