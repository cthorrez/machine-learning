import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

import algorithms.visualize.Plot as visualize

# import data set
dataset = pd.read_csv('resources/02_6_Position_Salaries.csv')
independentVariables = dataset.iloc[:, 1:2].values
dependantVariables = dataset.iloc[:, 2].values

# split into training and test sets (80:20)
# n/a: small dataset

# remove feature bias (i.e. feature scaling)
# n/a: applied by LinearRegression library already

# build model
polyFeatures = PolynomialFeatures(degree=4)
modelPolynomial = LinearRegression()
modelPolynomial.fit(polyFeatures.fit_transform(independentVariables), dependantVariables)

# check predictions (manually)
dPrediction = modelPolynomial.predict(polyFeatures.fit_transform(6.5))

# visualize model (2-dimensions)
visualize.plotPolynomialRegression(modelPolynomial, polyFeatures, independentVariables, independentVariables,
                                   dependantVariables, "Exp.", "$$$", "Polynomial Model")
visualize.plotPolynomialRegressionHD(modelPolynomial, polyFeatures, independentVariables, independentVariables, 0.1,
                                     dependantVariables, "Exp.", "$$$", "Polynomial Model")

