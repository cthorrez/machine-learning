import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.formula.api as sm
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

import algorithms.regression.strategy.BackwardElimination as backwardElimination

# import data set
dataset = pd.read_csv('resources/02_5_50_Startups.csv')
iVariables = dataset.iloc[:, :-1].values
dVariables = dataset.iloc[:, 4].values

# encode categorical variable to enum index
colIdx = 3
iLabelEncoder = LabelEncoder()
iVariables[:, colIdx] = iLabelEncoder.fit_transform(iVariables[:, colIdx])
iOneHotEncoder = OneHotEncoder(categorical_features=[colIdx])
iVariables = iOneHotEncoder.fit_transform(iVariables).toarray()

# remove multi-collinearity (i.e. Dummy Variable Trap)
iVariables = iVariables[:, 1:]

# split into training and test sets (80:20)
iTrain, iTest, dTrain, dTest = train_test_split(iVariables, dVariables, test_size=0.2, random_state=0)

# remove feature bias (i.e. feature scaling)
# not required for multiple linear regression. The library does it for free

# build model
regressor = LinearRegression()
regressor.fit(iTrain, dTrain)

# check predictions (manually)
dPrediction = regressor.predict(iTest)
deltaPrediction = dPrediction - dTest

# Backward Elimination - optimize model by removing insignificant features
iVariables = BackwardElimination.prependConstantColumn(iVariables)
iFeatureSet = [0, 1, 2, 3, 4, 5]
iFeaturesAll = iVariables[:, iFeatureSet]

model = BackwardElimination.generateOLSModel(iFeaturesAll, dVariables)
optimalModel = BackwardElimination.optimizeModel(model, iVariables, iFeatureSet, dVariables, 0.05)
optimalModel.summary()
optimalModel.r2
