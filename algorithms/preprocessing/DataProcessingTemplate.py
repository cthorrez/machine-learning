import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.cross_validation import train_test_split as TrainTestSplit
from sklearn.preprocessing import StandardScaler

""" Import the data set
    X: independent variables (must be as a matrix)
    y: dependant variable (i.e. result) """
dataset = pd.read_csv('resources/01_Data.csv')
iVariables = dataset.iloc[:, :-1].values
dVariables = dataset.iloc[:, 3].values

""" Handle missing (null) data """
imputer = Imputer(missing_values='NaN', strategy='mean', axis=0)
imputer = imputer.fit(iVariables[:, 1:3])
iVariables[:, 1:3] = imputer.transform(iVariables[:, 1:3])

""" Categorical data labels (i.e. enumerations)
    colIdx: column index of feature we are handling
    X: use LabelEncoder to convert enum int value, then use OneHotEncoder to split enumeration into boolean features
    y: similar (not necessary to use OneHotEncoder, since it is output boolean in this example) """
colIdx = 0
iLabelEncoder = LabelEncoder()
iVariables[:, colIdx] = iLabelEncoder.fit_transform(iVariables[:, colIdx])
iOneHotEncoder = OneHotEncoder(categorical_features=[colIdx])
iVariables = iOneHotEncoder.fit_transform(iVariables).toarray()
dLabelEncoder = LabelEncoder()
dVariables = dLabelEncoder.fit_transform(dVariables)

""" Categorical data labels (Remove multi-collinearity (i.e. Dummy Variable Trap) """
# iVariables = iVariables[:, 1:]

""" Training (80%) & Test (20%) dataset split """
iTrain, iTest, dTrain, dTest = TrainTestSplit(iVariables, dVariables, test_size=0.2, random_state=0)

""" Feature scaling (some model libraries do this for you, so may not be necessary to standardize data ranges)
    Scalar: fit and apply to the training dataset (to derive S.D). Only need to apply to test set """
iScalar = StandardScaler()
iTrain = iScalar.fit_transform(iTrain)
iTest = iScalar.transform(iTest)
