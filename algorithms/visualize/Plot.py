import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import ListedColormap


def plotLinearRegression(regressor, xTrain, xPoints, yPoints, xLabel, yLabel, title):
    """

    :param regressor: LinearRegression model
    :param xTrain: Independent variable training data set (used to build model line)
    :param xPoints: x-axis value data points (for plots to be scattered)
    :param yPoints: y-axis value data points (for plots to be scattered)
    :param xLabel: x-axis label
    :param yLabel: y-axis label
    :param title: title
    :return:
    """
    plt.scatter(xPoints, yPoints, color='red')
    plt.plot(xTrain, regressor.predict(xTrain), color='blue')
    plt.title(title)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.show()


def plotPolynomialRegression(regressor, polynomialFeatures, xTrain, xPoints, yPoints, xLabel, yLabel, title):
    """

    :param regressor: LinearRegression model fitted to PolynomialFeatures
    :param polynomialFeatures: instance of PolynomialFeatures()
    :param xTrain: Independent variable training data set (used to build model line)
    :param xPoints: x-axis value data points (for plots to be scattered)
    :param yPoints: y-axis value data points (for plots to be scattered)
    :param xLabel: x-axis label
    :param yLabel: y-axis label
    :param title: title
    :return:
    """
    plt.scatter(xPoints, yPoints, color='red')
    plt.plot(xTrain, regressor.predict(polynomialFeatures.fit_transform(xTrain)), color='blue')
    plt.title(title)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.show()


def plotPolynomialRegressionHD(regressor, polynomialFeatures, xTrain, xPoints, xIncrement, yPoints, xLabel, yLabel, title):
    """

    :param regressor: LinearRegression model fitted to PolynomialFeatures
    :param polynomialFeatures: instance of PolynomialFeatures()
    :param xTrain: Independent variable training data set (used to build model line)
    :param xPoints: x-axis value data points (for plots to be scattered)
    :param xIncrement: x-axis distance between each point
    :param yPoints: y-axis value data points (for plots to be scattered)
    :param xLabel: x-axis label
    :param yLabel: y-axis label
    :param title: title
    :return:
    """
    xGrid = np.arange(min(xTrain), max(xTrain), xIncrement)
    xGrid = xGrid.reshape((len(xGrid)), 1)

    plotPolynomialRegression(regressor, polynomialFeatures, xGrid, xPoints, yPoints, xLabel, yLabel, title)


def plotClassification2D(model, iVariables, dVariables, title='title', xLabel='x-axis', yLabel='y-axis'):
    plt.clf()
    borderSize = 1

    # Build matrix of variable ranges (i.e. all the pixels we want in the frame)
    i1VarRange, i2VarRange = np.meshgrid(
        np.arange(start=iVariables[:, 0].min() - borderSize, stop=iVariables[:, 0].max() + borderSize, step=0.01),
        np.arange(start=iVariables[:, 1].min() - borderSize, stop=iVariables[:, 1].max() + borderSize, step=0.01))

    # Plot the separation boundary predicted by the model
    plt.contourf(i1VarRange, i2VarRange,
                 model.predict(np.array([i1VarRange.ravel(), i2VarRange.ravel()]).T).reshape(
                     i1VarRange.shape),
                 alpha=0.75, cmap=ListedColormap(('red', 'green')))

    plt.xlim(i1VarRange.min(), i1VarRange.max())
    plt.ylim(i2VarRange.min(), i2VarRange.max())

    # Plot the observed values in dataset
    for i, j in enumerate(np.unique(dVariables)):
        plt.scatter(iVariables[dVariables == j, 0], iVariables[dVariables == j, 1],
                    c=ListedColormap(('red', 'green'))(i), label=j)

    plt.title(title)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.legend()
    return plt

