import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from algorithms.associationrulelearning.AprioriFunction import apriori

""""
Apriori Algorithm - basic recommender - recommend based on past event(s)

Can you improve prediction based on prior knowledge?

Example - Netflix:
    Rule = If a user has liked movie1 then are they likely to like movie2

    Support - population that supports the rule:
        percentage of total users who liked movie1
        (e.g. 10%)

    Confidence - population that supports the rule:
        number of users who liked both movie1 and movie2
        divided by
        number of users who liked movie1
        (e.g. 17.5%)

    Lift - what is the increase in likely hood by following this rule opposed to random selection
    Lift = Confidence / Support
    (e.g. 1.75) so 1.75 times more likely like movie2 if they have liked movie1 (over random selection)

"""

# import data set (as a list of lists)
dataset = pd.read_csv('resources/Market_Basket_Optimisation.csv', header=None)
transactionList = []
for i in range(0, 7501):
    transaction = [str(dataset.values[i, j]) for j in range(0, 20)]
    transactionList.append(transaction)

# train Apriori on dataset
rules = apriori(transactionList, min_support=0.003, min_confidence=0.2, min_lift=3, min_length=2)

# visualize results
results = list(rules)
print(results[0])
